﻿using RSICommons.CommonModels.DTO;
using RSICommons.CommonModels.DTO.Enums;
using RSICommons.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Admin_Desktop
{
    public partial class MainMenu : Form
    {
        public IEnumerable<Product> products { get; set; }
        public MainMenu()
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback += (se, cert, chain, sslerror) => { return true; };
            InitializeComponent();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            AddOrEditChooseBox editBox = new AddOrEditChooseBox();
            editBox.Owner = this;
            editBox.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            editBox.ShowDialog();
            if (editBox.result)
            {
                RefressList();
            }

            editBox.Dispose();
        }

        private async void MainMenu_Load(object sender, EventArgs e)
        {
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            EditProduct.Enabled = false;
            deleteProduct.Enabled = false;


            RefressList();
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {

        }

        private async void button1_Click_1(object sender, EventArgs e)
        {

            RefressList();
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            Product p = null;
            try
            {
                p = products.ElementAt(dataGridView1.CurrentRow.Index);
            }
            catch (Exception)
            {
            }
            if (p != null)
            {
                EditProduct.Enabled = true;
                deleteProduct.Enabled = true;
            }
        }

        private void EditProduct_Click(object sender, EventArgs e)
        {
            var p = products.ElementAt(dataGridView1.CurrentRow.Index);

            AddOrEditChooseBox editBox = new AddOrEditChooseBox(p);
            editBox.Owner = this;
            editBox.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            editBox.ShowDialog();
            if (editBox.result)
            {
                RefressList();
            }

            editBox.Dispose();
        }

        private async void deleteProduct_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Delete rekord?", "Confirm", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                var p = products.ElementAt(dataGridView1.CurrentRow.Index);

                var auth = TokenHelper.ReadTokenFromFile();
                auth = string.IsNullOrEmpty(auth) ? TokenHelper.DefaultToken : auth;

                var apiResponse = await ApiHelper.DeleteFromApi(auth, Commons.CrudLink, DTOTypes.Product.ToString(), new List<Guid> { p.Id });

                if (apiResponse.status == false)
                {
                    MessageBox.Show(apiResponse.result.ToString());
                }
                else
                    MessageBox.Show("Deleted!");

                RefressList();
            }
        }
        private async void RefressList()
        {
            var auth = TokenHelper.ReadTokenFromFile();
            auth = string.IsNullOrEmpty(auth) ? TokenHelper.DefaultToken : auth;

            products = await ApiHelper.GetItemsFromApi(auth, Commons.CrudLink, new Product(), DTOTypes.Product.ToString(), 0.ToString(), 0.ToString());
            if (products == null)
                MessageBox.Show("problem z komunikacjaz api");
            dataGridView1.DataSource = products;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SoldProducts sp = new SoldProducts();
            sp.Owner = this;
            sp.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            sp.ShowDialog();

            if (sp.result)
            {
                RefressList();
            }

            sp.Dispose();
        }
    }
}