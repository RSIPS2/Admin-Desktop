﻿using Newtonsoft.Json;
using RSICommons.CommonModels.DTO;
using RSICommons.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Admin_Desktop
{
    public partial class AddOrEditChooseBox : Form
    {
        private Product currentProduct { get; set; }
        public bool result { get; set; } = false;

        public AddOrEditChooseBox()
        {
            InitializeComponent();
            currentProduct = new Product();
        }

        public AddOrEditChooseBox(Product product)
        {
            currentProduct = product;
            InitializeComponent();
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, ItemCheckEventArgs e)
        {
            for (int ix = 0; ix < bookList.Items.Count; ++ix)
                if (ix != e.Index) bookList.SetItemChecked(ix, false);

            var product = (bookList.SelectedItem as CheckListViewModel)?.Value;

            titleTextBox.Text = product.Title;
            authorTextBox.Text = product.Author;
            descriptionTextBox.Text = product.Description;
            publisherTextBox.Text = product.Publisher;
            publishDateTextBox.Text = product.PublishDate;
            pagesCountTextBox.Value = product.PageCount;
            genreTextBox.Text = product.Genre;
            languageTextBox.Text = product.Language;
            originalLanguageTextBox.Text = product.OriginalLanguage;
            thumbnailTextBox.Text = product.Thumbnail;
            translatedByTextBox.Text = product.TranslatedBy;
        }
        private void AddOrEditChooseBox_Load(object sender, EventArgs e)
        {
            searchAmount.Value = 30;
            comboBox1.DisplayMember = "Display";
            comboBox1.DisplayMember = "Value";

            comboBox1.DataSource = new List<ComboBoxViewModel> { new ComboBoxViewModel("Polish", "pl"), new ComboBoxViewModel("English", "en") };

            bookList.DisplayMember = "Display";
            bookList.ValueMember = "Value";
            if (currentProduct.Id != Guid.Empty)
            {
                searchButton.Enabled = false;
                searchAuthorTextBox.Enabled = false;
                searchTitleTextBox.Enabled = false;
                titleTextBox.Text = currentProduct.Title;
                authorTextBox.Text = currentProduct.Author;
                descriptionTextBox.Text = currentProduct.Description;
                publisherTextBox.Text = currentProduct.Publisher;
                publishDateTextBox.Text = currentProduct.PublishDate;
                pagesCountTextBox.Value = currentProduct.PageCount;
                genreTextBox.Text = currentProduct.Genre;
                languageTextBox.Text = currentProduct.Language;
                originalLanguageTextBox.Text = currentProduct.OriginalLanguage;
                thumbnailTextBox.Text = currentProduct.Thumbnail;
                translatedByTextBox.Text = currentProduct.TranslatedBy;
                amountTextBox.Value = currentProduct.Amount;
                priceTextBox.Text = currentProduct.Price;
            }
        }

        private async void searchButton_Click(object sender, EventArgs e)
        {
            bookList.Items.Clear();

            var list = new List<Product>();
            var auth = TokenHelper.ReadTokenFromFile();
            auth = string.IsNullOrEmpty(auth) ? TokenHelper.DefaultToken : auth;
            var products = await ApiHelper.Get(auth, Commons.ScrapingLink, "/api/GetProductInformation", ((ComboBoxViewModel)comboBox1.SelectedItem).Display, searchAmount.Value.ToString(),  searchTitleTextBox.Text, searchAuthorTextBox.Text);
            if (products.StatusCode == System.Net.HttpStatusCode.OK)
            {
                list = JsonConvert.DeserializeObject<List<Product>>(await products.Content.ReadAsStringAsync());
                bookList.Items.Clear();
            }
            else if (products.StatusCode == System.Net.HttpStatusCode.NoContent)
            {
                bookList.Items.Clear();
                MessageBox.Show("No entries");
            }
            else if (products.StatusCode == System.Net.HttpStatusCode.RequestTimeout)
            {
                bookList.Items.Clear();
                MessageBox.Show("Timeout");
            }
            foreach (var item in list)
            {
                bookList.Items.Add(new CheckListViewModel(item,$"{item.Title} {item.Author}"), false);
            }
        }

        private async void saveButton_Click(object sender, EventArgs e)
        {

            currentProduct.Title = titleTextBox.Text;
            currentProduct.Author = authorTextBox.Text;
            currentProduct.Description = descriptionTextBox.Text;
            currentProduct.Publisher = publisherTextBox.Text;
            currentProduct.PublishDate = publishDateTextBox.Text;
            currentProduct.PageCount = (int)pagesCountTextBox.Value;
            currentProduct.Genre = genreTextBox.Text;
            currentProduct.Language = languageTextBox.Text;
            currentProduct.OriginalLanguage = originalLanguageTextBox.Text;
            currentProduct.Thumbnail = thumbnailTextBox.Text;
            currentProduct.TranslatedBy = translatedByTextBox.Text;
            currentProduct.Amount = (int)amountTextBox.Value;
            currentProduct.Price = priceTextBox.Text;

            var auth = TokenHelper.ReadTokenFromFile();
            auth = string.IsNullOrEmpty(auth) ? TokenHelper.DefaultToken : auth;
            var apiResponse = await ApiHelper.PostToApi(auth, Commons.CrudLink, "/api/AddOrUpdate", RSICommons.CommonModels.DTO.Enums.DTOTypes.Product.ToString(), currentProduct);
            if (apiResponse.status == false)
            {
                MessageBox.Show(apiResponse.result.ToString());
            }

            MessageBox.Show("Added/Updated!");
            result = true;
            this.Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AddOrEditChooseBox_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }

    public class CheckListViewModel
    {
        public Product Value { get; set; }
        public string Display { get; set; }

        public CheckListViewModel(Product p, string d)
        {
            Value = p;
            Display = d;
        }
    }

    public class ComboBoxViewModel
    {
        public string Value { get; set; }
        public string Display { get; set; }

        public ComboBoxViewModel(string v, string d)
        {
            Value = v;
            Display = d;
        }
    }
}
