﻿using Newtonsoft.Json;
using RSICommons.CommonModels.DTO;
using RSICommons.CommonModels.DTO.Enums;
using RSICommons.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Admin_Desktop
{
    public partial class SoldProducts : Form
    {
        public bool result { get; set; }
        public List<SoldProductsViewModel> vmList { get; set; }
        private Person Person { get; set; }

        public SoldProducts()
        {
            InitializeComponent();
        }

        private async void button2_Click(object sender, EventArgs e)
        {
            var p = vmList.ElementAt(dataGridView1.CurrentRow.Index);

            DialogResult dialogResult = MessageBox.Show("Delete rekord?", "Confirm", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                var auth = TokenHelper.ReadTokenFromFile();
                auth = string.IsNullOrEmpty(auth) ? TokenHelper.DefaultToken : auth;

                var apiResponse = await ApiHelper.DeleteFromApi(auth, Commons.CrudLink, DTOTypes.PersonProduct.ToString(), new List<Guid> { p.Id });

                if (apiResponse.status == false)
                {
                    MessageBox.Show(apiResponse.result.ToString());
                }
                MessageBox.Show("Deleted!");
                RefressList();
            }
        }

        private void SoldProducts_Load(object sender, EventArgs e)
        {

            dataGridView1.DataSource = null;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var p = vmList.ElementAt(dataGridView1.CurrentRow.Index);

            ReturnOrRemove returnOrRemove = new ReturnOrRemove(p);
            returnOrRemove.Owner = this;
            returnOrRemove.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            returnOrRemove.ShowDialog();
            if (returnOrRemove.result)
            {
                try
                {
                    RefressList();
                }
                catch (Exception ex)
                {
                }
            }

            returnOrRemove.Dispose();
        }

        private async void searchButton_Click(object sender, EventArgs e)
        {
            var auth = TokenHelper.ReadTokenFromFile();
            auth = string.IsNullOrEmpty(auth) ? TokenHelper.DefaultToken : auth;

            var response = await ApiHelper.Get(auth, Commons.CrudLink, "/api/GetUserByName", firstNameSearchBox.Text);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                try
                {
                    Person = JsonConvert.DeserializeObject<Person>(await response.Content.ReadAsStringAsync());
                    RefressList();
                }
                catch (Exception ex)
                {

                }
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.NoContent)
            {
                MessageBox.Show("No user Found");
                return;
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.RequestTimeout)
            {
                MessageBox.Show("Timeout");
                return;
            }

            auth = TokenHelper.ReadTokenFromFile();
            auth = string.IsNullOrEmpty(auth) ? TokenHelper.DefaultToken : auth;

            var products = await ApiHelper.GetItemsFromApi<PersonProduct>(auth, Commons.CrudLink, new PersonProduct(), DTOTypes.PersonProduct.ToString(), "0", "0", Person.Id.ToString());

        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {

        }

        private async void RefressList()
        {
            var auth = TokenHelper.ReadTokenFromFile();
            auth = string.IsNullOrEmpty(auth) ? TokenHelper.DefaultToken : auth;

            var products = await ApiHelper.GetItemsFromApi<PersonProduct>(auth, Commons.CrudLink, new PersonProduct(), DTOTypes.PersonProduct.ToString(), "0", "0", Person.Id.ToString());
            if (products == null)
            {
                MessageBox.Show("No books found");
                return;
            }
            vmList = new List<SoldProductsViewModel>();
            foreach (var item in products)
            {
                item.Person = Person;
                vmList.Add(new SoldProductsViewModel()
                {
                    Amount = item.Amount,
                    Author = item.Product.Author,
                    Price = item.Price,
                    BuyTime = item.BuyTime,
                    Id = item.Id,
                    PersonID = Person.Id,
                    ProductID = item.ProductId,
                    ProductName = item.Product.Title,
                    Publisher = item.Product.Publisher
                });
            }
            dataGridView1.DataSource = vmList;
        }
    }
    public class SoldProductsViewModel
    {
        [System.ComponentModel.Browsable(false)]
        public Guid Id { get; set; }

        [System.ComponentModel.Browsable(false)]
        public Guid PersonID { get; set; }
        [System.ComponentModel.Browsable(false)]
        public Guid ProductID { get; set; }
        public string ProductName { get; set; }
        public string Author { get; set; }
        public string Price { get; set; }
        public string Publisher { get; set; }
        public int Amount { get; set; }
        public DateTime BuyTime { get; set; }
    }
}
