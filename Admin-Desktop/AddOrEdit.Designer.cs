﻿namespace Admin_Desktop
{
    partial class AddOrEditChooseBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.searchTitleTextBox = new System.Windows.Forms.TextBox();
            this.titleLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Publisher = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.priceTextBox = new System.Windows.Forms.TextBox();
            this.genreTextBox = new System.Windows.Forms.TextBox();
            this.translatedByTextBox = new System.Windows.Forms.TextBox();
            this.thumbnailTextBox = new System.Windows.Forms.TextBox();
            this.originalLanguageTextBox = new System.Windows.Forms.TextBox();
            this.languageTextBox = new System.Windows.Forms.TextBox();
            this.pagesCountTextBox = new System.Windows.Forms.NumericUpDown();
            this.publishDateTextBox = new System.Windows.Forms.TextBox();
            this.publisherTextBox = new System.Windows.Forms.TextBox();
            this.amountTextBox = new System.Windows.Forms.NumericUpDown();
            this.descriptionTextBox = new System.Windows.Forms.TextBox();
            this.authorTextBox = new System.Windows.Forms.TextBox();
            this.titleTextBox = new System.Windows.Forms.TextBox();
            this.searchLabel = new System.Windows.Forms.Label();
            this.searchButton = new System.Windows.Forms.Button();
            this.bookList = new System.Windows.Forms.CheckedListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.saveButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.searchAuthorTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.searchAmount = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pagesCountTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.amountTextBox)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchAmount)).BeginInit();
            this.SuspendLayout();
            // 
            // searchTitleTextBox
            // 
            this.searchTitleTextBox.Location = new System.Drawing.Point(45, 16);
            this.searchTitleTextBox.Name = "searchTitleTextBox";
            this.searchTitleTextBox.Size = new System.Drawing.Size(194, 20);
            this.searchTitleTextBox.TabIndex = 0;
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.Location = new System.Drawing.Point(6, 33);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(30, 13);
            this.titleLabel.TabIndex = 1;
            this.titleLabel.Text = "Title:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(281, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Author:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Description:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(281, 229);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Publish date:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 281);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Language:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(73, 383);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Price:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(312, 307);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Genre:";
            // 
            // Publisher
            // 
            this.Publisher.AutoSize = true;
            this.Publisher.Location = new System.Drawing.Point(6, 229);
            this.Publisher.Name = "Publisher";
            this.Publisher.Size = new System.Drawing.Size(53, 13);
            this.Publisher.TabIndex = 8;
            this.Publisher.Text = "Publisher:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 253);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "Pages:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(260, 281);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(96, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "Original Language:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 333);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(59, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "Thumbnail:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 307);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(74, 13);
            this.label12.TabIndex = 12;
            this.label12.Text = "Translated by:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(305, 383);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(46, 13);
            this.label13.TabIndex = 13;
            this.label13.Text = "Amount:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.priceTextBox);
            this.groupBox1.Controls.Add(this.genreTextBox);
            this.groupBox1.Controls.Add(this.translatedByTextBox);
            this.groupBox1.Controls.Add(this.thumbnailTextBox);
            this.groupBox1.Controls.Add(this.originalLanguageTextBox);
            this.groupBox1.Controls.Add(this.languageTextBox);
            this.groupBox1.Controls.Add(this.pagesCountTextBox);
            this.groupBox1.Controls.Add(this.publishDateTextBox);
            this.groupBox1.Controls.Add(this.publisherTextBox);
            this.groupBox1.Controls.Add(this.amountTextBox);
            this.groupBox1.Controls.Add(this.descriptionTextBox);
            this.groupBox1.Controls.Add(this.authorTextBox);
            this.groupBox1.Controls.Add(this.titleTextBox);
            this.groupBox1.Controls.Add(this.titleLabel);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.Publisher);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Location = new System.Drawing.Point(12, 43);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(543, 414);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Product";
            // 
            // priceTextBox
            // 
            this.priceTextBox.Location = new System.Drawing.Point(113, 380);
            this.priceTextBox.Name = "priceTextBox";
            this.priceTextBox.Size = new System.Drawing.Size(138, 20);
            this.priceTextBox.TabIndex = 26;
            // 
            // genreTextBox
            // 
            this.genreTextBox.Location = new System.Drawing.Point(357, 304);
            this.genreTextBox.Name = "genreTextBox";
            this.genreTextBox.Size = new System.Drawing.Size(180, 20);
            this.genreTextBox.TabIndex = 25;
            // 
            // translatedByTextBox
            // 
            this.translatedByTextBox.Location = new System.Drawing.Point(86, 304);
            this.translatedByTextBox.Name = "translatedByTextBox";
            this.translatedByTextBox.Size = new System.Drawing.Size(186, 20);
            this.translatedByTextBox.TabIndex = 24;
            // 
            // thumbnailTextBox
            // 
            this.thumbnailTextBox.Location = new System.Drawing.Point(71, 330);
            this.thumbnailTextBox.Name = "thumbnailTextBox";
            this.thumbnailTextBox.Size = new System.Drawing.Size(468, 20);
            this.thumbnailTextBox.TabIndex = 23;
            // 
            // originalLanguageTextBox
            // 
            this.originalLanguageTextBox.Location = new System.Drawing.Point(357, 278);
            this.originalLanguageTextBox.Name = "originalLanguageTextBox";
            this.originalLanguageTextBox.Size = new System.Drawing.Size(180, 20);
            this.originalLanguageTextBox.TabIndex = 22;
            // 
            // languageTextBox
            // 
            this.languageTextBox.Location = new System.Drawing.Point(68, 278);
            this.languageTextBox.Name = "languageTextBox";
            this.languageTextBox.Size = new System.Drawing.Size(186, 20);
            this.languageTextBox.TabIndex = 21;
            // 
            // pagesCountTextBox
            // 
            this.pagesCountTextBox.Location = new System.Drawing.Point(52, 251);
            this.pagesCountTextBox.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.pagesCountTextBox.Name = "pagesCountTextBox";
            this.pagesCountTextBox.Size = new System.Drawing.Size(120, 20);
            this.pagesCountTextBox.TabIndex = 20;
            // 
            // publishDateTextBox
            // 
            this.publishDateTextBox.Location = new System.Drawing.Point(355, 226);
            this.publishDateTextBox.Name = "publishDateTextBox";
            this.publishDateTextBox.Size = new System.Drawing.Size(182, 20);
            this.publishDateTextBox.TabIndex = 19;
            // 
            // publisherTextBox
            // 
            this.publisherTextBox.Location = new System.Drawing.Point(65, 226);
            this.publisherTextBox.Name = "publisherTextBox";
            this.publisherTextBox.Size = new System.Drawing.Size(186, 20);
            this.publisherTextBox.TabIndex = 18;
            // 
            // amountTextBox
            // 
            this.amountTextBox.Location = new System.Drawing.Point(357, 381);
            this.amountTextBox.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.amountTextBox.Name = "amountTextBox";
            this.amountTextBox.Size = new System.Drawing.Size(120, 20);
            this.amountTextBox.TabIndex = 17;
            // 
            // descriptionTextBox
            // 
            this.descriptionTextBox.Location = new System.Drawing.Point(9, 83);
            this.descriptionTextBox.Multiline = true;
            this.descriptionTextBox.Name = "descriptionTextBox";
            this.descriptionTextBox.Size = new System.Drawing.Size(528, 123);
            this.descriptionTextBox.TabIndex = 16;
            // 
            // authorTextBox
            // 
            this.authorTextBox.Location = new System.Drawing.Point(328, 30);
            this.authorTextBox.Name = "authorTextBox";
            this.authorTextBox.Size = new System.Drawing.Size(209, 20);
            this.authorTextBox.TabIndex = 15;
            // 
            // titleTextBox
            // 
            this.titleTextBox.Location = new System.Drawing.Point(42, 30);
            this.titleTextBox.Name = "titleTextBox";
            this.titleTextBox.Size = new System.Drawing.Size(209, 20);
            this.titleTextBox.TabIndex = 14;
            // 
            // searchLabel
            // 
            this.searchLabel.AutoSize = true;
            this.searchLabel.Location = new System.Drawing.Point(9, 19);
            this.searchLabel.Name = "searchLabel";
            this.searchLabel.Size = new System.Drawing.Size(30, 13);
            this.searchLabel.TabIndex = 15;
            this.searchLabel.Text = "Title:";
            // 
            // searchButton
            // 
            this.searchButton.Location = new System.Drawing.Point(780, 14);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(75, 23);
            this.searchButton.TabIndex = 16;
            this.searchButton.Text = "Search";
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // bookList
            // 
            this.bookList.CheckOnClick = true;
            this.bookList.FormattingEnabled = true;
            this.bookList.Location = new System.Drawing.Point(18, 19);
            this.bookList.Name = "bookList";
            this.bookList.Size = new System.Drawing.Size(333, 379);
            this.bookList.TabIndex = 18;
            this.bookList.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.checkedListBox1_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.bookList);
            this.groupBox2.Location = new System.Drawing.Point(565, 43);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(357, 414);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "ChooseItem";
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(760, 475);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 20;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(841, 475);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 21;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // searchAuthorTextBox
            // 
            this.searchAuthorTextBox.Location = new System.Drawing.Point(292, 16);
            this.searchAuthorTextBox.Name = "searchAuthorTextBox";
            this.searchAuthorTextBox.Size = new System.Drawing.Size(176, 20);
            this.searchAuthorTextBox.TabIndex = 22;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(245, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 23;
            this.label1.Text = "Author:";
            // 
            // searchAmount
            // 
            this.searchAmount.Location = new System.Drawing.Point(526, 17);
            this.searchAmount.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.searchAmount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.searchAmount.Name = "searchAmount";
            this.searchAmount.Size = new System.Drawing.Size(39, 20);
            this.searchAmount.TabIndex = 24;
            this.searchAmount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(474, 19);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 13);
            this.label8.TabIndex = 25;
            this.label8.Text = "Amount:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(580, 19);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(58, 13);
            this.label14.TabIndex = 26;
            this.label14.Text = "Language:";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(644, 16);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(110, 21);
            this.comboBox1.TabIndex = 27;
            // 
            // AddOrEditChooseBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(934, 512);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.searchAmount);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.searchAuthorTextBox);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.searchButton);
            this.Controls.Add(this.searchLabel);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.searchTitleTextBox);
            this.Name = "AddOrEditChooseBox";
            this.Text = "AddOrEdit";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AddOrEditChooseBox_FormClosed);
            this.Load += new System.EventHandler(this.AddOrEditChooseBox_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pagesCountTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.amountTextBox)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.searchAmount)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox searchTitleTextBox;
        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label Publisher;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label searchLabel;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.CheckedListBox bookList;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox titleTextBox;
        private System.Windows.Forms.TextBox descriptionTextBox;
        private System.Windows.Forms.TextBox authorTextBox;
        private System.Windows.Forms.NumericUpDown amountTextBox;
        private System.Windows.Forms.TextBox priceTextBox;
        private System.Windows.Forms.TextBox genreTextBox;
        private System.Windows.Forms.TextBox translatedByTextBox;
        private System.Windows.Forms.TextBox thumbnailTextBox;
        private System.Windows.Forms.TextBox originalLanguageTextBox;
        private System.Windows.Forms.TextBox languageTextBox;
        private System.Windows.Forms.NumericUpDown pagesCountTextBox;
        private System.Windows.Forms.TextBox publishDateTextBox;
        private System.Windows.Forms.TextBox publisherTextBox;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.TextBox searchAuthorTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown searchAmount;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}