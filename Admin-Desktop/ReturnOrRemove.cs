﻿using RSICommons.CommonModels.DTO;
using RSICommons.CommonModels.DTO.Enums;
using RSICommons.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Admin_Desktop
{
    public partial class ReturnOrRemove : Form
    {
        public bool result { get; set; }
        SoldProductsViewModel P;

        public ReturnOrRemove()
        {
            InitializeComponent();
        }

        public ReturnOrRemove(SoldProductsViewModel p) : this()
        {
            P = p;
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            var pp = new PersonProduct()
            {
                Id = P.Id,
                Amount = P.Amount - (int)numericUpDown1.Value,
                BuyTime = P.BuyTime,
                Price = P.Price,
                PersonId = P.PersonID,
                ProductId = P.ProductID
            };

            var auth = TokenHelper.ReadTokenFromFile();
            auth = string.IsNullOrEmpty(auth) ? TokenHelper.DefaultToken : auth;

            var apiResponse = await ApiHelper.PostToApi(auth, Commons.CrudLink, "/api/AddOrUpdate", RSICommons.CommonModels.DTO.Enums.DTOTypes.PersonProduct.ToString(), pp);
            if (apiResponse.status == false)
            {
                MessageBox.Show(apiResponse.result.ToString());
            }
            MessageBox.Show("Updated!");

            result = true;
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
            result = false;
        }

        private void ReturnOrRemove_Load(object sender, EventArgs e)
        {
            numericUpDown1.Maximum = P.Amount;
        }
    }
}
