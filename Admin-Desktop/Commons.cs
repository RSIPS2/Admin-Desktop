﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin_Desktop
{
    class Commons
    {
        public static string ScrapingLink { get; } = "https://scraping-rsi.azurewebsites.net"; //"https://localhost:44396";
        public static string AuditsLink { get; } = "https://localhost:44xd";
        public static string CrudLink { get; } = "https://crud-rsi.azurewebsites.net"; //"http://localhost:11752";
    }
}
